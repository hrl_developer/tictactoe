var q_step = 'X';
var cols = document.getElementsByClassName('col');
var is_active_col = true;
var counter = 1;

function checkWinner(q_step, counter) {
	if (cols[0].textContent == q_step && cols[1].textContent == q_step && cols[2].textContent == q_step) {is_active_col = false;}
	if (cols[3].textContent == q_step && cols[4].textContent == q_step && cols[5].textContent == q_step) {is_active_col = false;}
	if (cols[6].textContent == q_step && cols[7].textContent == q_step && cols[8].textContent == q_step) {is_active_col = false;}
	if (cols[0].textContent == q_step && cols[3].textContent == q_step && cols[6].textContent == q_step) {is_active_col = false;}
	if (cols[1].textContent == q_step && cols[4].textContent == q_step && cols[7].textContent == q_step) {is_active_col = false;}
	if (cols[2].textContent == q_step && cols[5].textContent == q_step && cols[8].textContent == q_step) {is_active_col = false;}
	if (cols[0].textContent == q_step && cols[4].textContent == q_step && cols[8].textContent == q_step) {is_active_col = false;}
	if (cols[2].textContent == q_step && cols[4].textContent == q_step && cols[6].textContent == q_step) {is_active_col = false;}

	if (!is_active_col) {
		document.getElementById('score').innerHTML = `
			<span class="${q_step}">Выиграли ${q_step}</span>
		`;
	}

	if (counter == 9 && is_active_col == true) {
		document.getElementById('score').innerHTML = `
			<span>Ничья</span>
		`;
	}
}

for (var i = 0; i < cols.length; i++) {
	cols[i].onclick = function() {
		if (!this.textContent && is_active_col) {
			this.textContent = q_step;

			checkWinner(q_step, counter++);

			if (q_step == 'X') {
				q_step = 'O';
				this.style.color = '#ff5555';
			} else if (q_step = 'O') {
				q_step = 'X';
				this.style.color = '#85ff55';
			}

		}
	}
}